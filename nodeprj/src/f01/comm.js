const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');

router.get('/comm', (req, res) => {
    res.render('f01/comm');
});


/**設定Post方法 */

/**
 * 取得商品清單(Table)
 */
router.post('/getCommList', (req, res) => {
    console.log('getVenderList');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE CRE_USER = ? ';
    let params = [userId];
    if (postData.commNm) { 
        where += `AND COMM_NM Like ? `;
        params.push('%' + postData.commNm + '%');
    }
    if (postData.vendor) { 
        where += `AND VEN_ID = ? `;
        params.push(postData.vendor);
    }
    if (postData.status) { 
        where += `AND STATUS = ? `;
        params.push(postData.status);
    }
    

    let start = (page - 1) * rows;
    let sql1 = `SELECT COUNT(1) count FROM INMA_COMM ${where} `;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT A.COMM_ID, 
                               A.COMM_NM,
                               A.UNIT,
                               A.VEN_ID, 
                               (SELECT VEN_NM FROM INMA_VENDOR WHERE VEN_ID = A.VEN_ID) AS VEN_NM, 
                               A.STATUS, 
                               (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM
                        FROM INMA_COMM A ${where} ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 取得商品清單(Table)
 */
router.post('/getCommData', (req, res) => {
    console.log('getVenderData');
    const postData = req.body;
    
    let userId = req.session.loginId;

    let where = 'WHERE CRE_USER = ? ';
    let params = [userId];
    if (postData.commNm) { 
        where += `AND COMM_NM Like ? `;
        params.push('%' + postData.commNm + '%');
    }
    if (postData.vendor) { 
        where += `AND VEN_ID = ? `;
        params.push(postData.vendor);
    }
    if (postData.status) { 
        where += `AND STATUS = ? `;
        params.push(postData.status);
    }

    let sort = postData.sort ? postData.status : 'A.COMM_NM';
    params.push(sort);
    
    let sql1 = `SELECT A.COMM_ID, 
                       A.COMM_NM,
                       A.UNIT,
                       A.VEN_ID, 
                       (SELECT VEN_NM FROM INMA_VENDOR WHERE VEN_ID = A.VEN_ID) AS VEN_NM, 
                       A.STATUS, 
                       (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM
                FROM INMA_COMM A ${where} ORDER BY ${sort}`;
    db.queryAsync(sql1, params)
        .then(result => {
            res.json(result);
        });
});

/**
 * 新增商品
 */
router.post('/setCommData', (req, res) => {
    console.log('setVenderData');
    const postData = req.body;
    
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };
    let commNm = postData.commNm;
    let userId = req.session.loginId;
    let vendor = postData.vendor;
    let unit = postData.unit;
    console.log(postData);
    
    if (!commNm) {
        resend.code = 401;
        resend.message += "請輸入商品名稱\n";
    }
    if (!vendor) {
        resend.code = 401;
        resend.message += "請輸入進貨廠商\n";
    }
    if (!unit) {
        resend.code = 401;
        resend.message += "請輸入進貨單位\n";
    }

    if(resend.code == 401) {
        return res.json(resend);
    }
    
    let sql1 = `INSERT INTO INMA_COMM(COMM_NM, VEN_ID, UNIT, CRE_DT, CRE_USER ) 
                VALUES (?, ?, ?, sysdate(), ?)  `;
    let params = [commNm, vendor, unit, userId];
    db.queryAsync(sql1, params)
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '新增成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '新增異常';
            res.json(resend);
        });
    
});

router.post('/upCommData', (req, res) => {
    const postData = req.body;
    
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };
    let commNm = postData.commNm;
    let commId = postData.commId;
    let vendor = postData.vendor;
    let unit = postData.unit;
    let status = postData.status;
    console.log(postData);
    
    let updata = "";
    let params = [];
    if (commNm) {
        updata += ` COMM_NM = ?,`;
        params.push(commNm);
    }
    if (vendor) {
        updata += ` VEN_ID = ?,`;
        params.push(vendor);
    }
    if (unit) {
        updata += ` UNIT = ?,`;
        params.push(unit);
    }
    if (status) {
        updata += ` STATUS = ?,`;
        params.push(status);
    }
    if (!commId) {
        resend.code = 401;
        resend.message += "ID異常\n";
    }

    if(resend.code == 401) {
        return res.json(resend);
    }
    updata = updata.substring(0, updata.length - 1);

    
    let sql1 = `UPDATE INMA_COMM
                SET ${updata} 
                WHERE COMM_ID = ? `;
    params.push(commId);
    db.queryAsync(sql1, params)
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '修改成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '修改異常';
            res.json(resend);
        });
    
});



module.exports = router;