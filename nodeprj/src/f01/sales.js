const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');

router.get('/sales', (req, res) => {
    res.render('f01/sales');
});

router.post('/salitem', (req, res) => {
    const postData = req.body;
    let result = {
        type : postData.ftype,
        salId : postData.fSalId,
        init : postData.fInit
    }
    res.render('f01/salitem', result);
});


/**設定Post方法 */

/**
 * 取得進貨單(Table)
 */
router.post('/getSalesList', (req, res) => {
    console.log('getSalesList');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE A.CRE_USER = ? ';
    let params = [userId];
    if (postData.comm) { 
        where += `AND B.COMM_ID = ? `;
        params.push(postData.comm);
    }
    if (postData.vendor) { 
        where += `AND B.VEN_ID = ? `;
        params.push(postData.vendor);
    }
    if (postData.status) { 
        where += `AND A.STATUS = ? `;
        params.push(postData.status);
    }
    if (postData.dateS) { 
        where += `AND A.INIT_DT >= str_to_date(?,'%Y/%m/%d') `;
        params.push(postData.dateS);
    }
    if (postData.dateE) { 
        where += `AND A.INIT_DT <= str_to_date(?,'%Y/%m/%d') `;
        params.push(postData.dateE);
    }
    

    let start = (page - 1) * rows;
    let sql1 = `SELECT COUNT(1) count FROM ( SELECT DISTINCT A.SAL_ID FROM INMA_SALES A INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID ${where} ) C`;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT DISTINCT A.SAL_ID,
                               CONCAT('SAL_', A.SAL_ID) AS SAL,
                               date_format(A.INIT_DT,'%Y/%m/%d') AS INIT_DT,
                               A.STATUS,
                               A.REMARK,
                               (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM,
                               (SELECT SUM(AMOUNT * PRICE) FROM INMA_SALES_ITEM WHERE SAL_ID = A.SAL_ID) AS CASH
                        FROM INMA_SALES A
                        INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID
                        ${where} ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 取得銷貨清單
 */
router.post('/getSalItemList', (req, res) => {
    console.log('getSalItemList');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE A.CRE_USER = ? ';
    let params = [userId];
    if (postData.salId) { 
        where += `AND A.SAL_ID = ? `;
        params.push(postData.salId);
    }
    

    let start = (page - 1) * rows;
    let sql1 = `SELECT COUNT(1) count FROM INMA_SALES A ${where} `;
    console.log(sql1);
    console.log(params);
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT A.SAL_ID,
                               date_format(A.INIT_DT,'%Y/%m/%d') AS INIT_DT,
                               B.AMOUNT,
                               B.PRICE,
                               B.COMM_ID,
                               C.COMM_NM,
                               C.UNIT,
                               C.VEN_ID,
                               D.VEN_NM,
                               A.STATUS, 
                               (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM
                        FROM INMA_SALES A
                        INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID
                        INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
                        INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
                        ${where} ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 新增商品
 */
router.post('/setSales', (req, res) => {
    console.log('setSales');
    let userId = req.session.loginId;
    let data = JSON.parse(req.body.datas);
    let datas = data.data;
    let initDt = data.initDt;
    let remark = data.remark;
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };

    //資料檢核
    if (!initDt) resend.message += "請輸入銷貨日\n";
    if (datas.length == 0) resend.message += "請至少輸入一筆銷貨資料\n";
    if (resend.message) {
        resend.code = 401;
        resend.message = msg;
        return res.json(resend);
    }
    
    let sql1 = `INSERT INTO INMA_SALES(INIT_DT, CRE_DT, CRE_USER, REMARK) 
                VALUES (str_to_date(?,'%Y/%m/%d'), sysdate(), ?, ?)  `;
    let params = [initDt, userId, remark];
    db.queryAsync(sql1, params)
        .then(result => {
            let salId = result.insertId;
            let sql2 = `INSERT INTO INMA_SALES_ITEM (SAL_ID, COMM_ID, AMOUNT, PRICE, CRE_USER, CRE_DT) VALUES `;
            let params2 = [];
            for (let i in datas) {
                sql2 += `(?, ?, ?, ?, ?, sysdate()),`;
                params2.push(salId);
                params2.push(datas[i].COMM_ID);
                params2.push(datas[i].AMOUNT);
                params2.push(datas[i].PRICE);
                params2.push(userId);
            }
            sql2 = sql2.substring(0, sql2.length - 1);
            console.log(sql2);
            console.log(params2);
            return db.queryAsync(sql2, params2);
        })
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '新增成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '新增異常';
            res.json(resend);
        });
    
    

});

router.post('/upSalesData', (req, res) => {
    const postData = req.body;
    
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };

    let salId = postData.salId;
    let status = postData.status;
    console.log(postData);
    
    let updata = "";
    let params = [];
    if (status) {
        updata += ` STATUS = ?,`;
        params.push(status);
    }
    if (!salId) {
        resend.code = 401;
        resend.message += "ID異常\n";
    }

    if(resend.code == 401) {
        return res.json(resend);
    }
    updata = updata.substring(0, updata.length - 1);

    
    let sql1 = `UPDATE INMA_SALES
                SET ${updata} 
                WHERE SAL_ID = ? `;
    params.push(salId);
    db.queryAsync(sql1, params)
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '修改成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '修改異常';
            res.json(resend);
        });
    
});

/**
 * 確認是否可開啟銷貨單
 */
router.post('/checkOpnSal', (req, res) => {
    console.log('checkOpnSal');
    const postData = req.body;
    
    let userId = req.session.loginId;
    let salId = postData.salId;
    let resend = {
        code: 200,
        susses: true,
        message: ''
    };

    if (!salId) {
        resend.code = 401;
        resend.message += "ID異常\n";
        res.json(resend);
    }

    let where1 = 'WHERE A.STATUS = "01" AND A.CRE_USER = ? AND B.COMM_ID IN (SELECT COMM_ID FROM INMA_SALES_ITEM WHERE SAL_ID = ?)';
    let params1 = [userId, salId];
    let where2 = 'WHERE ( A.STATUS = "01" OR A.SAL_ID = ? ) AND A.CRE_USER = ? AND B.COMM_ID IN (SELECT COMM_ID FROM INMA_SALES_ITEM WHERE SAL_ID = ?)';
    let params2 = [salId, userId, salId];

    let sql1 = `
        SELECT A.COMM_ID, A.COMM_NM, SUM(A.AMOUNT) AS AMOUNT
        FROM (
            SELECT B.COMM_ID, C.COMM_NM, B.AMOUNT
            FROM INMA_PURCHASE A
            INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID
            INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
            ${where1}
        
            UNION ALL
        
            SELECT B.COMM_ID, C.COMM_NM, (B.AMOUNT * -1) AS AMOUNT
            FROM INMA_SALES A
            INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID
            INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
            ${where2}
        ) A
        GROUP BY A.COMM_ID, A.COMM_NM`;
    let params = params1.concat(params2);
    console.log(sql1);
    db.queryAsync(sql1, params)
        .then(result => {
            console.log(result);
            for(let i in result) {
                if (result[i].AMOUNT < 0) {
                    resend.code = 401;
                    resend.message += `${result[i].COMM_NM}庫存不足，無法重新啟用此銷貨單\n`;
                }
            }
            res.json(resend);
        });
});

module.exports = router;