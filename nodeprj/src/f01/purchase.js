const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');

router.get('/purchase', (req, res) => {
    res.render('f01/purchase');
});

router.post('/puritem', (req, res) => {
    const postData = req.body;
    let result = {
        type : postData.ftype,
        purId : postData.fPurId,
        init : postData.fInit,
    }
    res.render('f01/puritem', result);
});


/**設定Post方法 */

/**
 * 取得進貨單(Table)
 */
router.post('/getPurchaseList', (req, res) => {
    console.log('getPurchaseList');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE A.CRE_USER = ? ';
    let params = [userId];
    if (postData.comm) { 
        where += `AND B.COMM_ID = ? `;
        params.push(postData.comm);
    }
    if (postData.vendor) { 
        where += `AND B.VEN_ID = ? `;
        params.push(postData.vendor);
    }
    if (postData.status) { 
        where += `AND A.STATUS = ? `;
        params.push(postData.status);
    }
    if (postData.dateS) { 
        where += `AND A.INIT_DT >= str_to_date(?,'%Y/%m/%d') `;
        params.push(postData.dateS);
    }
    if (postData.dateE) { 
        where += `AND A.INIT_DT <= str_to_date(?,'%Y/%m/%d') `;
        params.push(postData.dateE);
    }
    

    let start = (page - 1) * rows;
    let sql1 = `SELECT COUNT(1) count FROM ( SELECT DISTINCT A.PUR_ID FROM INMA_PURCHASE A INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID ${where} ) C`;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT DISTINCT A.PUR_ID,
                               CONCAT('PUR_', A.PUR_ID) AS PUR,
                               date_format(A.INIT_DT,'%Y/%m/%d') AS INIT_DT,
                               A.STATUS,
                               (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM,
                               (SELECT SUM(AMOUNT * PRICE) FROM INMA_PURCHASE_ITEM WHERE PUR_ID = A.PUR_ID) AS CASH
                        FROM INMA_PURCHASE A
                        INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID
                        ${where} ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 取得進貨清單
 */
router.post('/getPurItemList', (req, res) => {
    console.log('getPurItemList');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE A.CRE_USER = ? ';
    let params = [userId];
    if (postData.purId) { 
        where += `AND A.PUR_ID = ? `;
        params.push(postData.purId);
    }
    

    let start = (page - 1) * rows;
    let sql1 = `SELECT COUNT(1) count FROM INMA_PURCHASE A ${where} `;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT A.PUR_ID,
                               date_format(A.INIT_DT,'%Y/%m/%d') AS INIT_DT,
                               B.AMOUNT,
                               B.PRICE,
                               B.COMM_ID,
                               C.COMM_NM,
                               C.UNIT,
                               C.VEN_ID,
                               D.VEN_NM,
                               A.STATUS, 
                               (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM
                        FROM INMA_PURCHASE A
                        INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID
                        INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
                        INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
                        ${where} ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 新增商品
 */
router.post('/setPurchase', (req, res) => {
    console.log('setPurchase');
    let userId = req.session.loginId;
    let data = JSON.parse(req.body.datas);
    let datas = data.data;
    let initDt = data.initDt;
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };

    //資料檢核
    if (!initDt) resend.message += "請輸入進貨日\n";
    if (datas.length == 0) resend.message += "請至少輸入一筆進貨資料\n";
    if (resend.message) {
        resend.code = 401;
        resend.message = msg;
        return res.json(resend);
    }
    
    let sql1 = `INSERT INTO INMA_PURCHASE(INIT_DT, CRE_DT, CRE_USER ) 
                VALUES (str_to_date(?,'%Y/%m/%d'), sysdate(), ?)  `;
    let params = [initDt, userId];
    db.queryAsync(sql1, params)
        .then(result => {
            let purId = result.insertId;
            let sql2 = `INSERT INTO INMA_PURCHASE_ITEM (PUR_ID, COMM_ID, AMOUNT, PRICE, CRE_USER, CRE_DT) VALUES `;
            let params2 = [];
            for (let i in datas) {
                sql2 += `(?, ?, ?, ?, ?, sysdate()),`;
                params2.push(purId);
                params2.push(datas[i].COMM_ID);
                params2.push(datas[i].AMOUNT);
                params2.push(datas[i].PRICE);
                params2.push(userId);
            }
            sql2 = sql2.substring(0, sql2.length - 1);
            return db.queryAsync(sql2, params2);
        })
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '新增成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '新增異常';
            res.json(resend);
        });
    
    

});

router.post('/upPurchaseData', (req, res) => {
    const postData = req.body;
    
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };

    let purId = postData.purId;
    let status = postData.status;
    
    let updata = "";
    let params = [];
    
    if (status) {
        updata += ` STATUS = ?,`;
        params.push(status);
    }
    if (!purId) {
        resend.code = 401;
        resend.message += "ID異常\n";
    }

    if(resend.code == 401) {
        return res.json(resend);
    }
    updata = updata.substring(0, updata.length - 1);

    
    let sql1 = `UPDATE INMA_PURCHASE
                SET ${updata} 
                WHERE PUR_ID = ? `;
    params.push(purId);
    db.queryAsync(sql1, params)
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '修改成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '修改異常';
            res.json(resend);
        });
    
});

/**
 * 確認是否可取消進貨單
 */
router.post('/checkDelPur', (req, res) => {
    console.log('checkDelPur');
    const postData = req.body;
    
    let userId = req.session.loginId;
    let purId = postData.purId;
    let resend = {
        code: 200,
        susses: true,
        message: ''
    };

    if (!purId) {
        resend.code = 401;
        resend.message += "ID異常\n";
        res.json(resend);
    }

    let where1 = 'WHERE A.STATUS = "01" AND A.CRE_USER = ? AND B.COMM_ID IN (SELECT COMM_ID FROM INMA_PURCHASE_ITEM WHERE PUR_ID = ?)';
    let params1 = [userId, purId];
    let where2 = where1;
    let params2 = params1;
    where1 += `AND A.PUR_ID != ?`;
    params1.push(purId);

    let sql1 = `
        SELECT A.COMM_ID, A.COMM_NM, SUM(A.AMOUNT) AS AMOUNT
        FROM (
            SELECT B.COMM_ID, C.COMM_NM, B.AMOUNT
            FROM INMA_PURCHASE A
            INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID
            INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
            ${where1}
        
            UNION ALL
        
            SELECT B.COMM_ID, C.COMM_NM, (B.AMOUNT * -1) AS AMOUNT
            FROM INMA_SALES A
            INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID
            INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
            ${where2}
        ) A
        GROUP BY A.COMM_ID, A.COMM_NM`;
    let params = params1.concat(params2);
    console.log(sql1);
    db.queryAsync(sql1, params)
        .then(result => {
            console.log(result);
            for(let i in result) {
                if (result[i].AMOUNT < 0) {
                    resend.code = 401;
                    resend.message += `${result[i].COMM_NM}已銷貨，無法取消\n`;
                }
            }
            res.json(resend);
        });
});


module.exports = router;