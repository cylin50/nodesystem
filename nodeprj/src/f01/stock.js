const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');

router.get('/stock', (req, res) => {
    res.render('f01/stock');
});

router.post('/stockitem', (req, res) => {
    const postData = req.body;
    let result = {
        commId : postData.commId,
        commNm : postData.commNm,
        venNm : postData.venNm
    }
    res.render('f01/stockitem', result);
});


/**設定Post方法 */

/**
 * 取得庫存清單(Table)
 */
router.post('/getStockList', (req, res) => {
    console.log('getStockList');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE A.CRE_USER = ? AND A.STATUS = ?';
    let params = [userId, '01'];
    if (postData.comm) { 
        where += `AND B.COMM_ID = ? `;
        params.push(postData.comm);
    }

    let baseSql = `
        SELECT
            A.COMM_ID,
            A.COMM_NM,
            A.VEN_ID,
            A.VEN_NM,
            SUM(A.PUR_CON) AS PUR_CON,
            SUM(A.PUR_SUM) AS PUR_SUM,
            SUM(A.SAL_CON) AS SAL_CON,
            SUM(A.SAL_SUM) AS SAL_SUM,
            SUM(A.STOCK) AS STOCK
        FROM (
            SELECT 
                B.COMM_ID,
                C.COMM_NM,
                C.VEN_ID,
                D.VEN_NM,
                SUM(B.AMOUNT) AS PUR_CON,
                SUM(B.AMOUNT * B.PRICE) AS PUR_SUM,
                0 AS SAL_CON,
                0 AS SAL_SUM,
                SUM(B.AMOUNT) AS STOCK
            FROM INMA_PURCHASE A
            INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID
            INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
            INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
            ${where}
            GROUP BY B.COMM_ID, C.COMM_NM, C.UNIT, C.VEN_ID, D.VEN_NM

            UNION

            SELECT 
                B.COMM_ID,
                C.COMM_NM,
                C.VEN_ID,
                D.VEN_NM,
                0 AS PUR_CON,
                0 AS PUR_SUM,
                SUM(B.AMOUNT) AS SAL_CON,
                SUM(B.AMOUNT * B.PRICE) AS SAL_SUM,
                SUM(B.AMOUNT * -1) AS STOCK
            FROM INMA_SALES A
            INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID
            INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
            INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
            ${where}
            GROUP BY B.COMM_ID, C.COMM_NM, C.UNIT, C.VEN_ID, D.VEN_NM
        ) A
        GROUP BY A.COMM_ID, A.COMM_NM, A.VEN_ID, A.VEN_NM
    `;
    params = params.concat(params);

    let start = (page - 1) * rows;
    let sql1 = `SELECT  COUNT(1) count FROM (  ${baseSql}  ) C`;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT * FROM ( ${baseSql} ) A ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 取得進銷清單(Table)
 */
router.post('/getStockItem', (req, res) => {
    console.log('getStockItem');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE A.CRE_USER = ? AND A.STATUS = ?';
    let params = [userId, '01'];
    if (postData.comm) { 
        where += `AND B.COMM_ID = ? `;
        params.push(postData.comm);
    }

    let baseSql = `
        SELECT 
            CONCAT('PUR_', A.PUR_ID) AS STOCK,
            'PUR' AS STOCK_TYPE,
            B.COMM_ID,
            C.COMM_NM,
            C.VEN_ID,
            D.VEN_NM,
            B.AMOUNT,
            B.PRICE,
            date_format(A.INIT_DT,'%Y/%m/%d') AS INIT_DT
        FROM INMA_PURCHASE A
        INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID
        INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
        INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
        ${where}

        UNION ALL

        SELECT 
            CONCAT('SAL_', A.SAL_ID) AS STOCK,
            'SAL' AS STOCK_TYPE,
            B.COMM_ID,
            C.COMM_NM,
            C.VEN_ID,
            D.VEN_NM,
            (B.AMOUNT * -1) AS AMOUNT,
            B.PRICE,
            date_format(A.INIT_DT,'%Y/%m/%d') AS INIT_DT
        FROM INMA_SALES A
        INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID
        INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
        INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
        ${where}
    `;
    params = params.concat(params);

    let start = (page - 1) * rows;
    let sql1 = `SELECT  COUNT(1) count FROM (  ${baseSql}  ) C`;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT * FROM ( ${baseSql} ) A ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 取得商品庫存數量
 */
router.post('/getCommStock', (req, res) => {
    console.log('getCommStock');
    const postData = req.body;
    
    let userId = req.session.loginId;

    let where = 'WHERE A.STATUS = "01" AND A.CRE_USER = ? ';
    let params = [userId];
    if (postData.commId) { 
        where += `AND B.COMM_ID = ? `;
        params.push(postData.commId);
    }
    

    let sql1 = `SELECT
                    A.COMM_ID,
                    A.COMM_NM,
                    A.UNIT,
                    A.VEN_ID,
                    A.VEN_NM,
                    SUM(A.AMOUNTS) AS AMOUNTS
                FROM (
                    SELECT SUM(B.AMOUNT) AS AMOUNTS,
                        B.COMM_ID,
                        C.COMM_NM,
                        C.UNIT,
                        C.VEN_ID,
                        D.VEN_NM
                    FROM INMA_PURCHASE A
                    INNER JOIN INMA_PURCHASE_ITEM B ON A.PUR_ID = B.PUR_ID
                    INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
                    INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
                    ${where}
                    GROUP BY B.COMM_ID, C.COMM_NM, C.UNIT, C.VEN_ID, D.VEN_NM
                    
                    UNION
                    
                    SELECT SUM(B.AMOUNT*-1) AS AMOUNTS,
                        B.COMM_ID,
                        C.COMM_NM,
                        C.UNIT,
                        C.VEN_ID,
                        D.VEN_NM
                    FROM INMA_SALES A
                    INNER JOIN INMA_SALES_ITEM B ON A.SAL_ID = B.SAL_ID
                    INNER JOIN INMA_COMM C ON B.COMM_ID = C.COMM_ID
                    INNER JOIN INMA_VENDOR D ON C.VEN_ID = D.VEN_ID
                    ${where}
                    GROUP BY B.COMM_ID, C.COMM_NM, C.UNIT, C.VEN_ID, D.VEN_NM
                ) A
                GROUP BY A.COMM_ID, A.COMM_NM, A.UNIT, A.VEN_ID, A.VEN_NM`;
    params = params.concat(params);
    db.queryAsync(sql1, params)
        .then(result => {
            res.json(result);
        });
});


module.exports = router;