const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');

router.get('/vendor', (req, res) => {
    res.render('f01/vendor');
});


/**設定Post方法 */

/**
 * 取得廠商清單(Table)
 */
router.post('/getVendorList', (req, res) => {
    console.log('getVendorList');
    const postData = req.body;
    
    let resend = {};
    let page = postData.page || 1;
    let rows = postData.rows || 10;
    let sort = postData.sort;
    let order = postData.order;
    let userId = req.session.loginId;

    let where = 'WHERE CRE_USER = ? ';
    let params = [userId];
    if (postData.venNm) { 
        where += `AND VEN_NM Like ? `;
        params.push('%' + postData.venNm + '%');
    }
    if (postData.status) { 
        where += `AND STATUS = ? `;
        params.push(postData.status);
    }
    

    let start = (page - 1) * rows;
    let sql1 = `SELECT COUNT(1) count FROM INMA_VENDOR ${where} `;
    db.queryAsync(sql1, params)
        .then(result => {
            resend.total = result[0].count;
            let sql2 = `SELECT 
                            A.VEN_ID, 
                            A.VEN_NM, 
                            A.TEL, 
                            A.ADDR, 
                            A.STATUS, 
                            (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM
                        FROM INMA_VENDOR A ${where} ORDER BY ${sort} ${order} LIMIT ${start}, ${rows}`;
            return db.queryAsync(sql2, params);
        })
        .then(result => {
            resend.rows = result;
            res.json(resend);
        });
});

/**
 * 取得廠商資料
 */
router.post('/getVendorData', (req, res) => {
    console.log('getVendorData');
    const postData = req.body;
    
    let userId = req.session.loginId;

    let where = 'WHERE CRE_USER = ? ';
    let params = [userId];
    if (postData.venNm) { 
        where += `AND VEN_NM Like ? `;
        params.push('%' + postData.venNm + '%');
    }
    if (postData.status) { 
        where += `AND STATUS = ? `;
        params.push(postData.status);
    }

    let sort = postData.sort ? postData.status : 'A.VEN_NM';
    params.push(sort);
    
    let sql1 = `SELECT A.VEN_ID, 
                       A.VEN_NM, 
                       A.TEL, 
                       A.ADDR, 
                       A.STATUS, 
                       (SELECT CD_NM FROM SYS_CD WHERE CT_ID = '01' AND CD_ID = A.STATUS) AS ST_NM 
                FROM INMA_VENDOR A ${where}  ORDER BY ${sort}`;
    db.queryAsync(sql1, params)
        .then(result => {
            res.json(result);
        });
});

/**
 * Insert廠商資料
 */
router.post('/setVendorData', (req, res) => {
    console.log('setVenderData');
    const postData = req.body;
    
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };
    let venNm = postData.venNm;
    let userId = req.session.loginId;
    let tel = postData.tel;
    let addr = postData.addr;
    console.log(postData);
    
    if (!venNm) {
        resend.code = 401;
        resend.message += "請輸入姓名\n";
    }
    if (!tel) {
        resend.code = 401;
        resend.message += "請輸入電話\n";
    }
    if (!addr) {
        resend.code = 401;
        resend.message += "請輸入地址\n";
    }

    if(resend.code == 401) {
        return res.json(resend);
    }
    
    let sql1 = `INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER ) 
                VALUES (?, ?, ?, sysdate(), ?)  `;
    let params = [venNm, tel, addr, userId];
    db.queryAsync(sql1, params)
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '新增成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '新增異常';
            res.json(resend);
        });
    
});

/**
 * update廠商資料
 */
router.post('/upVendorData', (req, res) => {
    console.log('setVenderData');
    const postData = req.body;
    
    let resend = {
        code: 400,
        susses: false,
        message: ''
    };
    let venNm = postData.venNm;
    let venId = postData.venId;
    let tel = postData.tel;
    let addr = postData.addr;
    let status = postData.status;
    
    let updata = "";
    let params = [];
    if (tel) {
        updata += ` TEL = ?,`;
        params.push(tel);
    }
    if (addr) {
        updata += ` ADDR = ?,`;
        params.push(addr);
    }
    if (status) {
        updata += ` STATUS = ?,`;
        params.push(status);
    }
    if (!venId) {
        resend.code = 401;
        resend.message += "ID異常\n";
    }

    if(resend.code == 401) {
        return res.json(resend);
    }
    updata = updata.substring(0, updata.length - 1);

    
    let sql1 = `UPDATE INMA_VENDOR
                SET ${updata} 
                WHERE VEN_ID = ? `;
    params.push(venId);
    db.queryAsync(sql1, params)
        .then(result => {
            resend.code = 200;
            resend.susses = true;
            resend.message = '修改成功';
            res.json(resend);
        })
        .error(error => {
            console.log(error);
            resend.code = 500;
            resend.message = '修改異常';
            res.json(resend);
        });
    
});



module.exports = router;