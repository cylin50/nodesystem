const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/../core/db_connection');

router.get('/monster', (req, res) => {
    res.render('f02/monsterStrike');
});

module.exports = router;