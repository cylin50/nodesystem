const mysql = require('mysql');
const bluebird = require('bluebird');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'gary',
    password: 'gary_113252',
    database: 'NODEDB'
});
bluebird.promisifyAll(db);

module.exports = db;