const express = require('express');
const router = express.Router();
const url = require('url');
const db = require(__dirname + '/db_connection');

router.get('/login', (req, res) => {
    res.render('core/login');
});

router.get('/logout', (req, res) => {
    delete req.session.isLogin;
    delete req.session.loginUser;
    delete req.session.menu;
    res.redirect('/login');
});

router.post('/login', (req, res) => {
    const data = req.body;
    let resdata = {};
    let loginId = data.loginId;
    let password = data.password;

    resdata.isLogin = false;

    let sql = `SELECT LOGIN_ID, USER_NM FROM SYS_USER WHERE UPPER(LOGIN_ID) = UPPER(?) AND PASSWORD = SHA1(?)`;
    let params = [loginId, password];
    db.queryAsync(sql, params)
        .then(result => {
            if(result[0]){
                req.session.isLogin = true;
                req.session.loginUser = result[0].USER_NM;
                req.session.loginId = result[0].LOGIN_ID;
                resdata.isLogin = true;
                resdata.redirectUrl = req.session.redirectUrl ? req.session.redirectUrl:'/';
                
                delete req.session.redirectUrl;
                let sql = `SELECT DISTINCT A.MENU_ID, A.MENU_NM, A.URI
                            FROM SYS_MENU A
                            INNER JOIN SYS_MENU B ON B.PAR_ID = A.MENU_ID
                            `;
                return db.queryAsync(sql);
            } else {
                resdata.msg = '帳號或密碼錯誤';
                res.json(resdata);
            }
        })
        .then(result => {
            if (result) {
                req.session.menu1 = result;
                let sql = `SELECT DISTINCT A.MENU_ID AS MID, A.MENU_NM AS MNM, B.MENU_ID, B.MENU_NM, B.URI
                            FROM SYS_MENU A
                            INNER JOIN SYS_MENU B ON B.PAR_ID = A.MENU_ID
                            `;
                return db.queryAsync(sql);
            }
        })
        .then(result => {
            if (result) {
                req.session.menu2 = result;
                let sql = `SELECT DISTINCT A.MENU_ID AS MID, A.MENU_NM AS MNM, B.MENU_ID, B.MENU_NM, B.URI
                            FROM SYS_MENU A
                            INNER JOIN SYS_MENU B ON B.PAR_ID = A.MENU_ID
                            `;
                res.json(resdata);
            }
        })
        .error(error => {
            console.log(error);
            resdata.msg = '系統異常';
            res.json(resdata);
        });
    
});

router.post('/loadSelectBox', (req, res) => {
    const data = req.body;
    let ctId = data.ctId;

    let sql = ` SELECT CD_ID, CD_NM FROM SYS_CD WHERE CT_ID = ? AND STATUS = '01' `;
    let params = [ctId];
    db.queryAsync(sql, params)
        .then(result => {
            res.json(result);
        })
        .error(error => {
            console.log(error);
            resdata.msg = '系統異常';
            res.json(resdata);
        });
    
});


module.exports = router;