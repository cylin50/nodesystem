const express = require('express');
const url = require('url');
const bodyParser = require('body-parser');
const session = require('express-session'); 
const moment = require('moment-timezone');
const db = require(__dirname + '/core/db_connection');

/**建立web Server物件 */
var app = express();

/**設定ejs頁面位置，非預設位置改為  app.set('views', '路徑'); */
app.set('view engine', 'ejs');

/**設定靜態頁面位置，非預設位置改為  app.use(express.static('路徑')); */
app.use(express.static('public'));
/**將body-parser設定在top middleware */
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
/**設定session參數 */
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret:'asddaassddassd',
    cookie: {
        maxAge: 600000
    }
}));

/**LOGIn Filter */
app.use((req, res, next) => {
    const urlParam = url.parse(req.url, true);

    if (req.session.isLogin) {
        res.locals.isLogin = req.session.isLogin;
        res.locals.loginUser = req.session.loginUser;
        res.locals.menu1 = req.session.menu1;
        res.locals.menu2 = req.session.menu2;
    }

    if (!req.session.isLogin && urlParam.href != '/login') {
        req.session.redirectUrl = urlParam.href;
        res.redirect('/login');
    }
    else if (req.session.isLogin && urlParam.href == '/login') {
        res.redirect('/');
    }
    else {
        next();
    }

});

/**載入外部連結 */
const test = require(__dirname + '/test');
app.use('/test', test);
const def = require(__dirname + '/core/def');
app.use(def);
const vendor = require(__dirname + '/f01/vendor');
app.use('/f01', vendor);
const comm = require(__dirname + '/f01/comm');
app.use('/f01', comm);
const purchase = require(__dirname + '/f01/purchase');
app.use('/f01', purchase);
const sales = require(__dirname + '/f01/sales');
app.use('/f01', sales);
const stock = require(__dirname + '/f01/stock');
app.use('/f01', stock);

const monsterStrike = require(__dirname + '/f02/monsterStrike');
app.use('/f02', monsterStrike);


/**設定路由 */
app.get('/', (req, res) => {
    res.render('index');
});



/**設定404頁面 */
app.use((req, res) => {
    res.type('text/plain');
    res.status(404);
    res.send('404 - 找不到網頁');
});

/**設定Port */
app.listen(3000, () => {
    console.log('Server Start Port:3000');
});