CREATE DATABASE NODEDB DEFAULT CHARACTER SET UTF8 COLLATE utf8_general_ci;
USE NODEDB;

drop TABLE if exists SYS_CT;
create table SYS_CT (
	CT_ID int not null auto_increment COMMENT '編號',
    CT_NM varchar(500) not null COMMENT '名稱',
    CRE_DT date not null COMMENT '建立日期',
    STATUS varchar(2) not null DEFAULT '01' COMMENT '狀態',
    PRIMARY KEY (CT_ID),
	UNIQUE KEY CT_ID (CT_ID)
);

drop TABLE if exists SYS_CD;
create table SYS_CD (
	CT_ID int not null COMMENT 'CT編號',
    CD_ID varchar(100) not null COMMENT 'CD代號',
    CD_NM varchar(500) not null COMMENT '名稱',
    CRE_DT date not null COMMENT '建立日期',
    STATUS varchar(2) not null DEFAULT '01' COMMENT '狀態',
    PRIMARY KEY (CT_ID, CD_ID),
	KEY STATUS (STATUS)
);

drop TABLE if exists SYS_MENU;
create table SYS_MENU (
	MENU_ID varchar(100) not null COMMENT 'ID',
    MENU_NM varchar(100) not null COMMENT '名稱',
    URI varchar(100) COMMENT 'URI',
    PAR_ID varchar(10) COMMENT '父類別編號',
    CRE_DT date not null COMMENT '建立日期',
    STATUS varchar(2) not null DEFAULT '01' COMMENT '狀態',
    PRIMARY KEY (MENU_ID),
	KEY PAR_ID (PAR_ID)
);

USE NODEDB;
TRUNCATE TABLE SYS_MENU;
INSERT INTO SYS_MENU(MENU_ID, MENU_NM, URI, CRE_DT)
VALUES('M01', '庫存管理', '', sysdate());
INSERT INTO SYS_MENU(MENU_ID, MENU_NM, URI, CRE_DT, PAR_ID)
VALUES('M0101', '廠商管理', 'f01/vendor', sysdate(), 'M01');
INSERT INTO SYS_MENU(MENU_ID, MENU_NM, URI, CRE_DT, PAR_ID)
VALUES('M0102', '商品管理', 'f01/comm', sysdate(), 'M01');
INSERT INTO SYS_MENU(MENU_ID, MENU_NM, URI, CRE_DT, PAR_ID)
VALUES('M0103', '進貨作業', 'f01/purchase', sysdate(), 'M01');
INSERT INTO SYS_MENU(MENU_ID, MENU_NM, URI, CRE_DT, PAR_ID)
VALUES('M0104', '銷貨作業', 'f01/sales', sysdate(), 'M01');
INSERT INTO SYS_MENU(MENU_ID, MENU_NM, URI, CRE_DT, PAR_ID)
VALUES('M0105', '庫存清單', 'f01/stock', sysdate(), 'M01');
COMMIT;

INSERT INTO SYS_CT(CT_ID, CT_NM, CRE_DT)
VALUES(1, '狀態', sysdate());
COMMIT;

INSERT INTO SYS_CD(CT_ID, CD_ID, CD_NM, CRE_DT)
VALUES(1, '01', '啟用', sysdate());
INSERT INTO SYS_CD(CT_ID, CD_ID, CD_NM, CRE_DT)
VALUES(1, '02', '停用', sysdate());
COMMIT;

DROP TABLE if exists INMA_VENDOR;
create table INMA_VENDOR (
	VEN_ID int not null auto_increment COMMENT 'VEN_ID',
    VEN_NM varchar(500) not null COMMENT '廠商名稱',
    TEL varchar(500) COMMENT '電話',
    ADDR varchar(2000) COMMENT '地址',
    CRE_USER varchar(20) not null COMMENT '建立人ID  Ref SYS_USER.LOGIN_ID',
    CRE_DT date not null COMMENT '建立日期',
	STATUS varchar(2) default'01' COMMENT '狀態 Ref SYS_CT : 01',
    PRIMARY KEY (VEN_ID),
	UNIQUE KEY `VEN_NM` (`VEN_NM`),
	KEY `STATUS` (`STATUS`)
);

/*測試資料*/
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('Apple', '02345678', '米國', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('台積電', '02345678', '台灣', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('鴻海', '02345678', '米國', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('廣達', '02345678', '台灣', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('晶華', '02345678', '台灣', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('三星', '02345678', '韓國', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('Android', '02345678', '米國', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('中鋼', '02345678', '台灣', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('資策會', '02345678', '台灣', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('巨匠', '02345678', '台灣', sysdate(), 'gary');
INSERT INTO INMA_VENDOR(VEN_NM, TEL, ADDR, CRE_DT, CRE_USER)
VALUES('頂新', '02345678', '台灣', sysdate(), 'gary');
COMMIT;

DROP TABLE if exists SYS_USER;
create table SYS_USER (
	LOGIN_ID varchar(20) not null COMMENT '登入帳號',
    PASSWORD varchar(40) not null COMMENT '密碼 SHA1',
    USER_NM varchar(500) not null COMMENT '姓名',
    USER_ID varchar(500) not null COMMENT '身分證字號',
    BIRTH date COMMENT '生日',
    MPHONE varchar(20) COMMENT '手機',
    HPHONE varchar(20) COMMENT '家裡電話',
    ADDR varchar(2000) COMMENT '地址',
    CRE_DT date not null COMMENT '建立日期',
	STATUS varchar(2) default'01' COMMENT '狀態 Ref SYS_CT : 01',
    PRIMARY KEY (LOGIN_ID),
	UNIQUE KEY `USER_ID` (`USER_ID`),
	KEY `STATUS` (`STATUS`)
);

INSERT INTO SYS_USER(LOGIN_ID, PASSWORD, USER_NM, USER_ID, CRE_DT)
VALUES('Gary', SHA1('Aa111111'), 'Gary', 'A111111111', sysdate());
COMMIT;

DROP TABLE if exists INMA_COMM;
create table INMA_COMM (
	COMM_ID int not null auto_increment COMMENT 'COMM_ID',
    COMM_NM varchar(500) not null COMMENT '商品名稱',
    VEN_ID int not null COMMENT '進貨廠商 Rrf INMA_VENDOR.VEN_ID',
    UNIT varchar(500) not null COMMENT '進貨單位',
    CRE_USER varchar(20) not null COMMENT '建立人ID  Ref SYS_USER.LOGIN_ID',
    CRE_DT date not null COMMENT '建立日期',
	STATUS varchar(2) default'01' COMMENT '狀態 Ref SYS_CT : 01',
    PRIMARY KEY (COMM_ID),
	KEY `STATUS` (`STATUS`),
    KEY `VEN_ID` (`VEN_ID`),
    KEY `CRE_USER` (`CRE_USER`)
);

DROP TABLE if exists INMA_PURCHASE;
create table INMA_PURCHASE (
	PUR_ID int not null auto_increment COMMENT '進貨單號',
    INIT_DT date not null COMMENT '進貨日期',
    CRE_USER varchar(20) not null COMMENT '建立人ID  Ref SYS_USER.LOGIN_ID',
    CRE_DT date not null COMMENT '建立日期',
	STATUS varchar(2) default'01' COMMENT '狀態 Ref SYS_CT : 01',
    PRIMARY KEY (PUR_ID),
	KEY `STATUS` (`STATUS`),
    KEY `INIT_DT` (`INIT_DT`),
    KEY `CRE_USER` (`CRE_USER`)
);
DROP TABLE if exists INMA_PURCHASE_ITEM;
create table INMA_PURCHASE_ITEM (
	PURITEM_ID int not null auto_increment COMMENT 'ID',
    PUR_ID int not null COMMENT '進貨單號 Ref INMA_PURCHASE.PUR_ID',
    COMM_ID varchar(500) not null COMMENT '商品ID Ref INMA_COMM.COMM_ID',
    AMOUNT int not null COMMENT '進貨數量',
    PRICE int not null COMMENT '進貨單價',
    CRE_USER varchar(20) not null COMMENT '建立人ID  Ref SYS_USER.LOGIN_ID',
    CRE_DT date not null COMMENT '建立日期',
	STATUS varchar(2) default'01' COMMENT '狀態 Ref SYS_CT : 01',
    PRIMARY KEY (PURITEM_ID),
	KEY `STATUS` (`STATUS`),
    KEY `PUR_ID` (`PUR_ID`),
    KEY `COMM_ID` (`COMM_ID`),
    KEY `CRE_USER` (`CRE_USER`)
);

DROP TABLE if exists INMA_SALES;
create table INMA_SALES (
	SAL_ID int not null auto_increment COMMENT '銷貨單號',
    INIT_DT date not null COMMENT '銷貨日期',
    REMARK varchar(2000)  COMMENT '備註',
    CRE_USER varchar(20) not null COMMENT '建立人ID  Ref SYS_USER.LOGIN_ID',
    CRE_DT date not null COMMENT '建立日期',
	STATUS varchar(2) default'01' COMMENT '狀態 Ref SYS_CT : 01',
    PRIMARY KEY (SAL_ID),
	KEY `STATUS` (`STATUS`),
    KEY `INIT_DT` (`INIT_DT`),
    KEY `CRE_USER` (`CRE_USER`)
);
DROP TABLE if exists INMA_SALES_ITEM;
create table INMA_SALES_ITEM (
	SALITEM_ID int not null auto_increment COMMENT 'ID',
    SAL_ID int not null COMMENT '銷貨單號 Ref INMA_SALES.SAL_ID',
    COMM_ID varchar(500) not null COMMENT '商品ID Ref INMA_COMM.COMM_ID',
    AMOUNT int not null COMMENT '銷貨數量',
    PRICE int not null COMMENT '銷貨單價',
    CRE_USER varchar(20) not null COMMENT '建立人ID  Ref SYS_USER.LOGIN_ID',
    CRE_DT date not null COMMENT '建立日期',
	STATUS varchar(2) default'01' COMMENT '狀態 Ref SYS_CT : 01',
    PRIMARY KEY (SALITEM_ID),
	KEY `STATUS` (`STATUS`),
    KEY `SAL_ID` (`SAL_ID`),
    KEY `COMM_ID` (`COMM_ID`),
    KEY `CRE_USER` (`CRE_USER`)
);